/*
//CREAR SERVIDOR POR NODE
const http =  require('http');

const server = http.createServer((req,res) =>{
    res.status = 200;
    res.setHeader('Content-type', 'text/html');
    res.end('<h1>Hola Mundo</h1>');
});

server.listen(3000, (err) =>{
    if(err){
        console.log(err)
    }
    console.log('cargando el servidor en el puerto 3000');
});
*/
//con esto se inicia un servidor en express
const express = require('express');
const morgan = require('morgan');
const app = express();



function logger(req, res, next){
    console.log(`Route Received: ${req.protocol}://${req.get('host')}${req.originalUrl}`);
    next();
}
//Settings
app.set('appName','Luis Curso');
app.set('port',5000);
//Middleware
app.use(express.json()); 
app.use(morgan('dev'))
app.use(logger);

app.get('/user',(req,res) => {
    res.json({
        username : 'triusdeil',
        lastname : 'luis ascanio'
    });
});

app.post('/user/:id',(req,res)=>{
    console.log(req.body);
    console.log(req.params);
    res.send('Peticion Post Recibido');
});
app.put('/user/:userId',(req,res)=>{
    console.log(req.body);
    res.send(`User ${req.params.userId} Updated`);
});

app.delete('/user/:userId',(req,res)=>{
    res.send(`User ${req.params.userId} deleted`);
});

app.use(express.static('public'));

app.listen(app.get('port'),() => {
    console.log(app.get('appName'));
    console.log("servidor en puerto:",app.get('port'));
});